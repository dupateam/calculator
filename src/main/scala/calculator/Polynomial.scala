package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    val be = b()
    val aa = a()
    val ce = c()
    Signal(be * be - 4 *  aa * ce)
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    val deltaVal = delta()
    if (deltaVal < 0) Signal( Set() )
    else {
      val aVal  = a()
      val bVal = b()
      val sqrtVal = math.sqrt(deltaVal)
      Signal(Set(
        (-1 * bVal + sqrtVal) / 2 * aVal,
        (-1 * bVal - sqrtVal) / 2 * aVal))
    }
  }
}
